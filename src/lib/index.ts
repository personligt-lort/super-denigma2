import Image from '$solvers/image.svelte';
import Morse from '$solvers/morse.svelte';
import NumberRep from '$solvers/numberRep.svelte';
import Shift from '$solvers/shift.svelte';
import Vignere from '$solvers/vigenere.svelte';
import Substition from '$solvers/substitution.svelte';

import type { ComponentType } from 'svelte';

export const solvers: {
	[key: string]: {
		title: string;
		solver: ComponentType;
	};
} = Object.freeze({
	image: { title: 'Image-based ciphers', solver: Image },
	morse: { title: 'Morse decode/encode', solver: Morse },
	numberRep: { title: 'Number-based representation', solver: NumberRep },
	shift: { title: 'Shift cipher', solver: Shift },
	vignere: { title: 'Vignere cipher', solver: Vignere },
	substition: { title: 'Mono-alphabetic substition', solver: Substition }
});
