export function countChars(str: string, alphabet = ''): { [key: string]: number } {
	const letterCounts: { [key: string]: number } = { total: 0 };

	const chars = [...str.toLowerCase()];
	for (const char of chars) {
		letterCounts['total'] += 1;

		if (Boolean(alphabet) && !alphabet.includes(char)) continue;

		const currentCount = letterCounts[char] ?? 0;
		letterCounts[char] = currentCount + 1;
	}

	return letterCounts;
}

export function summarizeCharCount(letterCounts: { [key: string]: number }): {
	[key: string]: number;
} {
	const totalCount = letterCounts['total'];

	return Object.fromEntries(
		Object.entries(letterCounts)
			.filter(([char, _]) => char != 'total')
			.map(([char, count]) => [char, (count / totalCount) * 100])
	);
}
