import { countChars, summarizeCharCount } from '../statistics';

import { danishAlphabet, danishFreq, englishAlphabet, englishFreq } from './lookups/alphabets';

export const subLangs = Object.freeze({
	Danish: {
		tableTemplate: sortBasedOnFreq(danishFreq),
		alphabet: danishAlphabet
	},
	English: {
		tableTemplate: sortBasedOnFreq(englishFreq),
		alphabet: englishAlphabet
	}
});

function sortBasedOnFreq(subTable: { [key: string]: number }) {
	return Object.entries(subTable)
		.sort(function (a, b) {
			return b[1] - a[1];
		})
		.map(([letter, _]) => letter);
}

function createSubTableFromArrays(
	alphabetArray: string[],
	userInputArray: string[]
): { [key: string]: string } {
	return Object.fromEntries(userInputArray.map((_, i) => [alphabetArray[i], userInputArray[i]]));
}

export function createRandomTable(alphabet: string) {
	let alphabetList = alphabet.split('');
	let shuffledAlphabetList = [...alphabetList].sort(() => 0.5 - Math.random());

	return createSubTableFromArrays(alphabetList, shuffledAlphabetList);
}

export function createTableFromTemplate(
	cipherText: string,
	subLang: { alphabet: string; tableTemplate: string[] },
	microShuffle = false
) {
	let cipherCharCount = summarizeCharCount(countChars(cipherText, subLang.alphabet));

	if (microShuffle) {
		for (const [key, value] of Object.entries(cipherCharCount)) {
			cipherCharCount[key] = value + value * (Math.random() < 0.5 ? 1 : 1) * Math.random();
		}
	}

	let sortedCipherChars = sortBasedOnFreq(cipherCharCount);

	return createSubTableFromArrays(sortedCipherChars, subLang.tableTemplate);
}

export function substitute(substitutionTable: { [key: string]: string }, cipherText: string) {
	const result = [];

	let index = 0;

	for (let char of cipherText) {
		let clearChar;
		let modified;

		const lowerChar = char.toLowerCase();

		if (lowerChar in substitutionTable && Boolean(substitutionTable[lowerChar])) {
			clearChar = substitutionTable[lowerChar];
			if (lowerChar !== char) {
				clearChar = clearChar.toUpperCase();
			}

			modified = true;
		} else {
			clearChar = char;
			modified = false;
		}
		result.push({ modified: modified, clearChar: clearChar, oldChar: char, index });
		index++;
	}
	return result;
}
