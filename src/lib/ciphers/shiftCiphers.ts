export function solveVigenere(cipherText: string, key: string, alphabet: string, encrypt = true) {
	let clearText = '';
	alphabet = alphabet.toLowerCase();
	key = key.toLowerCase();
	let keyIndex = 0;

	for (let cipherChar of cipherText) {
		let keyCharIndex = (encrypt ? 1 : -1) * alphabet.indexOf(key[keyIndex % key.length]);

		if (alphabet.includes(cipherChar.toLowerCase())) {
			clearText += shiftSingleChar(cipherChar, keyCharIndex, alphabet);
			keyIndex++;
		} else {
			clearText += cipherChar;
		}
	}
	return clearText;
}

export function enumerateShift(
	cipherText: string,
	alphabets: string[]
): Array<{ shiftCount: number; clearText: string }> {
	const shiftCount = Math.max(...alphabets.map((alphabet) => alphabet.length));
	const result = [];

	for (let i = 0; i < shiftCount; i++) {
		result.push({ shiftCount: i, clearText: solveShift(cipherText, i, alphabets) });
	}

	return result;
}
export function solveShift(cipherText: string, shiftCount: number, alphabets: string[]): string {
	let result = cipherText;

	for (const alphabet of alphabets) {
		result = shiftText(result, shiftCount, alphabet);
	}

	return result;
}

function shiftText(cipherText: string, shiftCount: number, alphabet: string): string {
	let clearText: string = '';
	alphabet = alphabet.toLowerCase();

	for (let char of cipherText) {
		clearText += shiftSingleChar(char, shiftCount, alphabet);
	}

	return clearText;
}

// Takes in all-lowercase aplhabet (as we are interrested only in the meaning and not specific characthers)
// But preserves case of shifted char
function shiftSingleChar(char: string, shiftCount: number, alphabet: string): string {
	let lowerChar = char.toLowerCase();

	if (alphabet.includes(lowerChar)) {
		let offSet = (alphabet.indexOf(lowerChar) + shiftCount) % alphabet.length;

		if (offSet < 0) {
			offSet = alphabet.length + offSet;
		}

		let clearChar = alphabet[offSet];

		if (lowerChar === char) {
			return clearChar;
		} else {
			return clearChar.toUpperCase();
		}
	} else {
		return char;
	}
}
