export type availableReps = 'Binary' | 'Decimal' | 'Hex';

export const nrReps: Array<availableReps> = ['Binary', 'Decimal', 'Hex'];

export const repLookup: { [key in availableReps]: number } = Object.freeze({
	Binary: 2,
	Decimal: 10,
	Hex: 16
});

export function numberToText(cipherText: string[], nrRepType: availableReps, alphabet = '') {
	return cipherText.map((nr) => {
		let parsedNr = parseInt(nr, repLookup[nrRepType]);

		if (!isNaN(parsedNr)) {
			if (Boolean(alphabet)) {
				if (parsedNr <= alphabet.length) {
					return alphabet[parsedNr - 1];
				} else {
					return parsedNr;
				}
			} else {
				return String.fromCharCode(parsedNr);
			}
		} else {
			return nr;
		}
	});
}

export function textToNumber(cipherText: string, nrRepType: availableReps, alphabet = '') {
	let numericRepresentations: Array<string> = [];

	const toString = (nr: number) => nr.toString(repLookup[nrRepType]);

	if (Boolean(alphabet)) {
		for (const char of cipherText) {
			let index = alphabet.indexOf(char) + 1;

			if (index > 0) {
				numericRepresentations.push(toString(index));
			} else {
				numericRepresentations.push(char);
			}
		}
	} else {
		for (const char of cipherText) {
			numericRepresentations.push(toString(char.charCodeAt(0)));
		}
	}

	return numericRepresentations;
}
