export function solveMorse(
	cipherText: string,
	alphabet: { [key: string]: string },
	prevDelim: string = '/',
	newDelim: string = '',
	notRecognized: string = ''
): string {
	let solution = '';

	let chars = cipherText.toLowerCase().split(prevDelim);

	for (const char of chars) {
		if (char in alphabet) {
			solution += alphabet[char] + newDelim;
		} else {
			if (Boolean(notRecognized)) {
				solution += notRecognized;
			} else {
				solution += char;
			}
		}
	}

	return solution;
}
