import { solvers } from '$lib';

import type { LayoutLoad } from './$types';

export const load: LayoutLoad = ({ params }) => {
	return {
		solver: { name: params.solver, ...solvers[params.solver] }
	};
};
