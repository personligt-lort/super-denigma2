import { solvers } from '$lib/index';

import type { ParamMatcher } from '@sveltejs/kit';

export const match: ParamMatcher = (param) => {
	return param in solvers;
};
